
using HtmlAgilityPack;
using LigaAlgorytmicznaSPOJNetCore.Data;
using LigaAlgorytmicznaSPOJNetCore.Database.Models;
using LigaAlgorytmicznaSPOJNetCore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace LigaAlgorytmicznaSPOJNetCore.Controllers
{
    
    public class DetailProblem
    {
        public string Text { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }

    }

    public class SharedController : Controller
    {
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();

        public string GetDetailProblemInfo(string link)
        {
            var url = "http://" + link;
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var text = getBetween(doc.ParsedText, "<div id=\"problem-body\">", "</div>");
            return text;
        }
        public ActionResult DetailProblems(string link)
        {
            var info = GetDetailProblemInfo(link);
            return View(info);
        }
        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        public static List<Problem> GetClassicalProblems()
        {
            var url = "http://www.spoj.com/problems/classical/";
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var list = new List<Problem>();
            foreach (var row in doc.DocumentNode.SelectNodes("//table//tbody"))
            {
                HtmlNodeCollection cells = row.SelectNodes("tr");
                if (cells != null)
                {
                    foreach (HtmlNode cell in cells)
                    {
                        HtmlNodeCollection kom = cell.SelectNodes("td");
                        if (kom != null)
                        {
                            var problem = new Problem();

                            int i = 0;

                            foreach (HtmlNode item in kom)
                            {
                                if (i == 0)
                                { problem.Id = item.InnerText.Trim(); }
                                else if (i == 1)
                                {
                                    problem.Name = item.InnerText.Trim();
                                    problem.Link = "www.spoj.com" + getBetween(item.InnerHtml, "<a href=\"", "\">");


                                }
                                else if (i == 2)
                                { problem.Quality = item.InnerText.Trim(); }
                                else if (i == 3)
                                { problem.Users = item.InnerText.Trim(); }
                                else if (i == 4)
                                { problem.Acc = item.InnerText.Trim(); }
                                else if (i == 5)
                                {
                                    problem.Difficulty = item.InnerText.Trim();
                                    list.Add(problem);
                                    i = -1;
                                }

                                i++;
                            }
                        }
                    }
                }
            }

            return list;
        }
        public static List<Problem> PolishProblems()
        {

            //        var url = "http://www.spoj.com/problems/classical/";
            var url = "https://pl.spoj.com/problems/latwe/";
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var list = new List<Problem>();
            foreach (var row in doc.DocumentNode.SelectNodes(@"//*[@id=""content""]/table[3]"))
            {
             var   a = row;
                HtmlNodeCollection cells = row.SelectNodes("tr");
                if (cells != null)
                {
                    foreach (HtmlNode cell in cells)
                    {
                        HtmlNodeCollection kom = cell.SelectNodes("td");
                        if (kom != null)
                        {
                            var problem = new Problem();

                            int i = 0;

                            foreach (HtmlNode item in kom)
                            {
                                if (i == 0)
                                { problem.Id = item.InnerText.Trim(); }
                                else if (i == 1)
                                {
                                    problem.Name = item.InnerText.Trim().Replace("&nbsp;\n&nbsp;", "");
                                    problem.Link = "www.pl.spoj.com/" + getBetween(item.InnerHtml, "<a href=\"", "\">");
                                }
                                else if (i == null)
                                { problem.Quality = item.InnerText.Trim(); }
                                else if (i == 3)
                                { problem.Users = item.InnerText.Trim(); }
                                else if (i == 4)
                                {
                                    problem.Acc = item.InnerText.Trim();
                                    list.Add(problem);
                                }
                                else if (i == 5)
                                {
                                    problem.Difficulty = item.InnerText.Trim();
                                    list.Add(problem);
                                    i = -1;
                                }

                                i++;
                            }
                        }
                    }
                }
            }

            return list;
        }
        public ActionResult ClassicalProblems()
        {
            var classicalProblems = GetClassicalProblems();

            return View(classicalProblems);
        }
        public ActionResult GetPolishProblems()
        {
            var classicalProblems = PolishProblems();
            return View("ClassicalProblems",classicalProblems);
        }
        
        public void Ocen(Guid nameofproblem)
        {
            var listUsers = new List<ApplicationUser>();
            listUsers = _dbContext.Users.Where(x => x.isConfirmed).ToList();
            var exercise = _dbContext.Exercises.FirstOrDefault(x => x.Id == nameofproblem);
            var listStudentWithMark = new List<StudentAndMark>();
            foreach (var item in listUsers)
            {
                var studentWithMark = new StudentAndMark();
                var mark = new Mark();
                mark = _dbContext.Marks.FirstOrDefault(x => x.Id_Person == item.Id && x.Id_Exercise == nameofproblem);
                studentWithMark.User = item;
                studentWithMark.Mark = mark;
                studentWithMark.NameExcercise = exercise.Name;
                studentWithMark.IdExercise = nameofproblem;

                listStudentWithMark.Add(studentWithMark);
            }
      //      listStudentWithMark.OrderByDescending(x => x.)
            foreach (var student in listStudentWithMark)
            {
      



            }


        }
   


    }
  

    public class UserRanking
    {
        public ApplicationUser User { get; set; }
        public List<Mark> Marks { get; set; }
        public int Score { get; set; }
        public UserRanking(ApplicationUser item, List<Mark> marks)
        {
            this.User = item;
            this.Marks = marks;
            Score = Marks.Sum(x => x.Score);
        }

   
    }
}
