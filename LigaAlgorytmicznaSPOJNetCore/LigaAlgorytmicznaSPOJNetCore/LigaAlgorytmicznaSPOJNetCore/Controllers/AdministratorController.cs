﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LigaAlgorytmicznaSPOJNetCore.Database.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using LigaAlgorytmicznaSPOJNetCore.Data;
using LigaAlgorytmicznaSPOJNetCore.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;

namespace LigaAlgorytmicznaSPOJNetCore.Controllers
{
  [Authorize(Roles = "Administrator")]
    public class AdministratorController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public AdministratorController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        ApplicationDbContext _dbContext=new ApplicationDbContext();
      
        public async Task<ActionResult> ShowPersons()
        {
            var list =await _userManager.GetUsersInRoleAsync("Student");     
            return View(list);
        }
        public ActionResult ShowExercisesToDo()
        {
            var list = _dbContext.Exercises.OrderByDescending(x=>x.DateToDo).ToList();
            return View(list);
        }

        public async Task<ActionResult> PersonalData()
        {
            var user =  await _userManager.GetUserAsync(HttpContext.User);
            return View(user);
        }
        public async Task<IActionResult> RankingView()
        {
          var  users=await _userManager.GetUsersInRoleAsync("Student");
           users=users.Where(x => x.isConfirmed).ToList();
            var list = new List<UserRanking>();
            foreach (var item in users)
            {
                var marks = new List<Mark>();
                marks = _dbContext.Marks.Where(x => x.Id_Person == item.Id).ToList();
                list.Add(new UserRanking(item, marks));
            }
            list = list.OrderByDescending(x => x.Score).ToList();
            return View(list);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPersonalData(IFormCollection formCollection)
        {
            var dbContext = new ApplicationDbContext();
            var userActive = await _userManager.GetUserAsync(HttpContext.User);
            userActive.PhoneNumber = formCollection["PhoneNumber"];
            userActive.Email = formCollection["Email"];
            userActive.UserName = formCollection["Email"];
            Task.WaitAny(_userManager.UpdateAsync(userActive));
            dbContext.SaveChanges();

            return RedirectToAction("PersonalData", "Administrator");
        }

        public ActionResult AddExercise()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddExerciseToDb(IFormCollection formCollection)
        {
            var link = formCollection["Name"];
            link = link.ToString().ToUpper();
            if (await ExercieseIsExistOnSpoj(link))
            {
            var exercise=new Exercise();            
            exercise.Name = link.ToString().Replace("www.spoj.com/problems/", "");
            exercise.isComplete = false;
            var str = formCollection["DateToDo"];
            exercise.DateToDo =Convert.ToDateTime(str);
            var dateWhenShow = formCollection["DateWhenShowForUsers"];
            exercise.DateWhenShowForUsers = Convert.ToDateTime(dateWhenShow);
            exercise.MaxScore = Convert.ToInt32(formCollection["Score"]);
            exercise.Score = Convert.ToInt32(formCollection["Score"]);
            _dbContext.Exercises.Add(exercise);
            _dbContext.SaveChanges();
            return RedirectToAction("ShowExercisesToDo","Administrator");
            }
            else
            {
                return AddExercise();
            }
        }
        
        private async Task<bool> ExercieseIsExistOnSpoj(string NameExercise)
        {
            var url = "https://www.spoj.com/problems/" + NameExercise;
            HttpClient hc = new HttpClient();
            HttpResponseMessage result = await hc.GetAsync(url);
            if (result.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        public ActionResult AddToGroup(string idToAdd)
        {
          var userToAdd=  _dbContext.Users.First(x => x.Id== idToAdd);
            userToAdd.isConfirmed = true;
            _dbContext.SaveChanges();
         return   RedirectToAction("ShowPersons");
        }
        
        [HttpPost]
        public ActionResult DeleteUserFromGroup(string idToDelete)
        {
            var userToDelete = _dbContext.Users.First(x => x.Id == idToDelete);
            _dbContext.Users.Remove(userToDelete);
            _dbContext.SaveChanges();

            return RedirectToAction("ShowPersons");
        }

        [HttpPost]
        public ActionResult AddMyGrades(IFormCollection formCollection)
        {
            var mark = new Mark();
            mark.Id_Exercise =new Guid (formCollection["IdExercise"]);
            mark.Id_Person = (formCollection["IdPerson"]);
            mark.Score =Convert.ToInt16(formCollection["Score"]);
            _dbContext.Marks.Add(mark);
            _dbContext.SaveChanges();
            return RedirectToAction("ManageMark","Administrator", new {nameofproblem=mark.Id_Exercise});
        }
        public async System.Threading.Tasks.Task<IActionResult> UserInformationAndProblemResolvedAsync(string nick)
        {
            var user = await _dbContext.Users.FirstAsync(x => x.Nick == nick);
            var marks = await _dbContext.Marks.FirstOrDefaultAsync(x => x.Id_Person == user.Id.ToString());

            var _Marklist = await GetMarkListAsync(user);
            var list = new List<MarkWithExercise>();
            foreach (var mark in _Marklist)
            {
                var item = new MarkWithExercise();
                item.Mark = mark;
                item.Exercise = _dbContext.Exercises.FirstOrDefault(x => x.Id == mark.Id_Exercise);
                list.Add(item);
            } 


            return View(list);
        }

        public async Task<List<Mark>> GetMarkListAsync(ApplicationUser user)
        {
            var list = _dbContext.Marks.Where(x => x.Id_Person == user.Id).ToList();
            return list;
        }

        public async Task<IActionResult> ManagePoints() {
            Points points = new Points();
            
            if (!_dbContext.ManagmentPoints.Any())
            {
                points.PointsForTheBest = new List<string>() { "0", "0" , "0", "0" };
            }
            else
            {
                var managmentPoint = await _dbContext.ManagmentPoints.OrderByDescending(x => x.DateWhenSet).FirstOrDefaultAsync();
                points.PointsForTheBest = new List<string>() {
                    managmentPoint.PointsForFirstPerson.ToString(),
                    managmentPoint.PointsForSecondPerson.ToString(),
                    managmentPoint.PointsForThirdPerson.ToString(),
                    managmentPoint.PointsForOtherPerson.ToString()};
            }
            return View(points);
        }
     
        [HttpPost]
        public async Task<IActionResult> ManagePoints(IFormCollection formCollection)
        {
            var pointsList = new List<string>();
            int first =Convert.ToInt32((formCollection["PointsForTheBest[0]"]));
            int second = Convert.ToInt32(formCollection["PointsForTheBest[1]"]);
            int third = Convert.ToInt32(formCollection["PointsForTheBest[2]"]);
            int other = Convert.ToInt32(formCollection["PointsForTheBest[3]"]);
            var managmentPoint = new ManagmentPoint()
            {
                DateWhenSet = DateTime.Now,
                PointsForFirstPerson = first,
                PointsForSecondPerson = second,
                PointsForThirdPerson = third,
                PointsForOtherPerson = other
            };
          await _dbContext.ManagmentPoints.AddAsync(managmentPoint);
          await  _dbContext.SaveChangesAsync();
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> DeleteExercise(Guid IdExercise) {
           var exerciseToDelete= await _dbContext.Exercises.FirstOrDefaultAsync(x => x.Id==IdExercise);
            _dbContext.Exercises.Remove(exerciseToDelete);
            await _dbContext.SaveChangesAsync();
           return RedirectToAction("ShowExercisesToDo","Administrator"); 
        }
        public async System.Threading.Tasks.Task<ActionResult> ManageMark(Guid nameofproblem)
        {
            var listAllUsers = await _userManager.GetUsersInRoleAsync("Student");
            var listUsers= listAllUsers.Where(x => x.isConfirmed);
            var exercise = _dbContext.Exercises.FirstOrDefault(x => x.Id == nameofproblem);
            var listStudentWithMark = new List<StudentAndMark>();

            foreach (var item in listUsers)
            {
                var studentWithMark = new StudentAndMark();
                var mark = new Mark();
                mark = _dbContext.Marks.FirstOrDefault(x => x.Id_Person == item.Id && x.Id_Exercise == nameofproblem);
                studentWithMark.User = item;
                studentWithMark.Mark = mark;
                studentWithMark.NameExcercise = exercise.Name;
                studentWithMark.IdExercise = nameofproblem;
                listStudentWithMark.Add(studentWithMark);
            }
            return View(listStudentWithMark);
        }   

    }
}