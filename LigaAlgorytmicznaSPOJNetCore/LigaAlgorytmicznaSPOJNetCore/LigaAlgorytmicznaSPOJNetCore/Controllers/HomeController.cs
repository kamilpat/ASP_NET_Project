﻿
using HtmlAgilityPack;
using LigaAlgorytmicznaSPOJNetCore.Areas.Identity.Pages.Account;
using LigaAlgorytmicznaSPOJNetCore.Data;
using LigaAlgorytmicznaSPOJNetCore.Database.Models;
using LigaAlgorytmicznaSPOJNetCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;

namespace LigaAlgorytmicznaSPOJNetCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        [AllowAnonymous]
        public IActionResult Register()
        {
            var model = new RegisterModel.InputModel();
            model.Nick = "";
            ViewBag.IsVerified = "";
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel.InputModel model)
        {

            if (ModelState.IsValid)
            {
                if (await NickExistOnSpoj(model.Nick))
                {
                    var user = new ApplicationUser
                {
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Gender = model.Gender.ToString(),
                    BirthDate = Convert.ToDateTime(model.BirthDate),
                    PhoneNumber = model.PhoneNumber,
                    Email = model.Email,
                    AlbumNumber = model.Id_Album,
                    Nick = model.Nick,
                };
                


                    var dbContext = new ApplicationDbContext();
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, "Student");
                        await _signInManager.SignInAsync(user, false);
                        return RedirectToAction("ToDo", "Student");
                    }

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View(model);
                }
            }
            return View(model);
        }

        private async Task<bool> NickExistOnSpoj(string nick)
        {
            var url = "https://www.spoj.com/users/" + nick;
            HttpClient hc = new HttpClient();
            HttpResponseMessage result = await hc.GetAsync(url);
                     if (result.IsSuccessStatusCode)
            {
                return true;
                    }
            else
            {
                return false;
                    }
        }

        public async Task<IActionResult> Index()
        {
            var u = await _userManager.GetUserAsync(HttpContext.User);
            if (u != null)
            {
                if (await _userManager.IsInRoleAsync(u, "Administrator"))
                {
                    return RedirectToAction("ShowExercisesToDo", "Administrator");
                }

                else if (await _userManager.IsInRoleAsync(u, "Student"))
                {
                    return RedirectToAction("ToDo", "Student");
                }
                else
                {
                    ModelState.AddModelError("", "Złe dane logowania");
                }
            }
            return View();
        }

       

        [HttpPost]
        public async Task<IActionResult> Logoff()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(IndexViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
        
             var result = await _signInManager.PasswordSignInAsync(model.Email.ToLower(), model.Password, model.RememberMe, false);
            if (result.Succeeded)
            {      
              return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Złe dane logowania");
                return View(model);
            }
            }
        public IActionResult ForgotPassword()
        { return View(new ForgotPasswordModel.InputModel()); }
        [HttpPost]
        public IActionResult ForgotPassword(ForgotPasswordModel.InputModel model)
        {

            var fromEmail = "ligaalgorytmicznaspoj@gmail.com";

            var username = model.Email;
                var user = _userManager.FindByEmailAsync(username).Result;

                if (user == null)
                {
                    ViewBag.Message = "Błąd podczas resetowania hasła";
                    return View("Error");
                }

                var token = _userManager.GeneratePasswordResetTokenAsync(user).Result;

                var resetLink = Url.Action("ResetPassword", "Home", new { token = token },
                                 protocol: HttpContext.Request.Scheme);


                        SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                MailMessage mailmsg = new MailMessage();

                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential(fromEmail, "zolza1234");

                mailmsg.From = new MailAddress(fromEmail);
                mailmsg.To.Add(username);
                mailmsg.Body = "Witaj użytkowniku ligi algorytmicznej ATH w Bielsku-Białej przed chwilą zostało wysłanie żadanie zmiany hasła:Aby je zmienić kliknij link obok " + resetLink;
                mailmsg.Subject = "Resetuj Hasło";
                mailmsg.IsBodyHtml = true;
                mailmsg.Priority = MailPriority.High;
                try
                {
                    smtp.Timeout = 500000;
                    smtp.Send(mailmsg);
                    mailmsg.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                ViewBag.Message = "Link resetujący hasło został do Ciebie wysłany";
                return View("Index");           
        }



        [HttpPost]
        public async Task<IActionResult> ChangePassword(IFormCollection formCollection)
        {
            var u = await _userManager.GetUserAsync(HttpContext.User);
            var token = _userManager.GeneratePasswordResetTokenAsync(u).Result;
            var haslo = formCollection["Haslo"];
            var nowehaslo = formCollection["NoweHaslo"];

            var result =  await _userManager.ChangePasswordAsync(u,haslo, nowehaslo);
            if (result.Succeeded)
            {
                ViewBag.Message = "Hasło zostało zresetowane prawidłowo!";
                return View("Index");
            }
            else
            {
                ViewBag.Message = "Błąd podczas resetowania hasła! Prawdopodobnie token nie aktywny";
                return View("Index");
            }
            
        }

        public IActionResult ResetPassword(string token)
        {
            return View();
        }
        [HttpPost]
        public IActionResult ResetPassword(ResetPasswordViewModel obj)
        {
            var user = _userManager.FindByEmailAsync(obj.UserName).Result;

            IdentityResult result = _userManager.ResetPasswordAsync(user, obj.Token, obj.Password).Result;
            if (result.Succeeded)
            {
                ViewBag.Message = "Hasło zostało zresetowane prawidłowo!";
                return View("Index");
            }
            else
            {
                ViewBag.Message = "Błąd podczas resetowania hasła! Prawdopodobnie token nie aktywny";
                return View();
            }
        }

    }
}

