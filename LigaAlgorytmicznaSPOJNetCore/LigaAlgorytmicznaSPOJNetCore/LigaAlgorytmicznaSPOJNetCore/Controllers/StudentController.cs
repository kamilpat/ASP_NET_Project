using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LigaAlgorytmicznaSPOJNetCore.Data;
using LigaAlgorytmicznaSPOJNetCore.Database.Models;
using LigaAlgorytmicznaSPOJNetCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LigaAlgorytmicznaSPOJNetCore.Controllers
{
    [Authorize(Roles = "Student")]
    public class StudentController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public StudentController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        ApplicationDbContext _dbContext = new ApplicationDbContext();
        
        public async Task<ActionResult> PersonalData()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);       
            return View(user);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPersonalData(IFormCollection formCollection)
        {
            var dbContext = new ApplicationDbContext();
            var userActive = await _userManager.GetUserAsync(HttpContext.User);         
            userActive.PhoneNumber = formCollection["PhoneNumber"];
            userActive.Email = formCollection["Email"];
            userActive.UserName = formCollection["Email"];
            Task.WaitAny(_userManager.UpdateAsync(userActive));
            dbContext.SaveChanges();

            return RedirectToAction("PersonalData", "Student");
        }
        
        public async Task<List<ResultProblem>> GetInfoAboutResultAsync(string link)
        {
            var url = "http://" + link;
           
            HttpClient hc = new HttpClient();
            HttpResponseMessage result = await hc.GetAsync(url);
            Stream stream = await result.Content.ReadAsStreamAsync();
            HtmlDocument docc = new HtmlDocument();
            docc.Load(stream);
          // var docc = web.LoadAsync(url);

          var list = new List<ResultProblem>();
            var doc= docc.DocumentNode.SelectNodes("//table//tbody");
            string xpath;
            if (doc == null)
            {
                 xpath ="//table";
            }
            else
            {
                xpath = "//table//tbody";
            }

            foreach (var row in docc.DocumentNode.SelectNodes(xpath))
                {
                    HtmlNodeCollection cells = row.SelectNodes("tr");
                    if (cells != null)
                    {
                        foreach (HtmlNode cell in cells)
                        {
                            HtmlNodeCollection kom = cell.SelectNodes("td");
                            if (kom != null)
                            {
                                var problem = new ResultProblem();

                                int i = 0;

                                foreach (HtmlNode item in kom)
                                {
                                    if (i == 0)
                                    { problem.Id = item.InnerText.Trim(); }
                                    else if (i == 1)
                                    {
                                        problem.Date = item.InnerText.Trim();
                                    }
                                    else if (i == 2)
                                    { problem.Problem = item.InnerText.Trim(); }
                                    else if (i == 3)
                                    { problem.Result = item.InnerText.Trim(); }
                                    else if (i == 4)
                                    { problem.Time = item.InnerText.Trim(); }
                                    else if (i == 5)
                                    {
                                        problem.Memory = item.InnerText.Trim();


                                    }
                                    else if (i == 6)
                                    {
                                        problem.Lang = item.InnerText.Trim();
                                        list.Add(problem);
                                        i = -1;
                                    }

                                    i++;
                                }
                            }
                        }
                    }
                }

            return list;

        }


        public async Task<List<ProblemWithLink>> UserParserProblemsResolveAsync(string username)
        {
            var url = "http://www.spoj.com/users/" + username + "/";
            HttpClient hc = new HttpClient();
            HttpResponseMessage result = await hc.GetAsync(url);
            Stream stream = await result.Content.ReadAsStreamAsync();
            HtmlDocument doc = new HtmlDocument();
            doc.Load(stream);

            var list = new List<ProblemWithLink>();
            foreach (var row in doc.DocumentNode.SelectNodes("//table"))
            {
                HtmlNodeCollection cells = row.SelectNodes("tr");
                if (cells == null)
                    continue;
                foreach (HtmlNode cell in cells)
                {
                    HtmlNodeCollection kom = cell.SelectNodes("td");
                    if (kom == null)
                        continue;
                    foreach (HtmlNode item in kom)
                    {
                        list.Add(new ProblemWithLink(item.InnerText, "www.spoj.com" + getBetween(item.OuterHtml, "<a href=\"", "\">")));
                    }
                }
            }
            return list;
        }
        public async Task<List<ProblemWithLink>> UserParserPolishProblemsResolve(string username)
        {
            var url = "https://pl.spoj.com/users/" + "witman" + "/";
            HttpClient hc = new HttpClient();
            HttpResponseMessage result = await hc.GetAsync(url);
            Stream stream = await result.Content.ReadAsStreamAsync();
            HtmlDocument doc = new HtmlDocument();
            doc.Load(stream);


            var list = new List<ProblemWithLink>();
            foreach (var row in doc.DocumentNode.SelectNodes(@"//*[@id=""content""]/div[1]/div[2]/table[1]"))
            {

                HtmlNodeCollection cells = row.SelectNodes("tr");
                if (cells == null)
                    continue;
                foreach (HtmlNode cell in cells)
                {
                    HtmlNodeCollection kom = cell.SelectNodes("td");
                    if (kom == null)
                        continue;
                    foreach (HtmlNode item in kom)
                    {
                        list.Add(new ProblemWithLink(item.InnerText, "pl.spoj.com" + getBetween(item.OuterHtml, "<a href=\"", "\">")));
                    }
                }
            }

            return list;
        }

        public async Task<ActionResult> ProblemsResolve()
        {
            var u = await _userManager.GetUserAsync(HttpContext.User);

            var user = u.Nick;
      var list= await UserParserProblemsResolveAsync(user);
            return View(list);
        }

        public async Task<ActionResult> GetPolishResolveProblems()
        {
            var u = await _userManager.GetUserAsync(HttpContext.User);
            var user =u.Nick;
            var list = await UserParserPolishProblemsResolve(user);
            return View("ProblemsResolve", list);
        }
        public async Task<IActionResult> RankingView()
        {
            var users = await _userManager.GetUsersInRoleAsync("Student");
            users = users.Where(x => x.isConfirmed).ToList();
            var list = new List<UserRanking>();
            foreach (var item in users)
            {
                var marks = new List<Mark>();
                marks = _dbContext.Marks.Where(x => x.Id_Person == item.Id).ToList();
                list.Add(new UserRanking(item, marks));
            }
            list = list.OrderByDescending(x => x.Score).ToList();
            return View(list);
        }
         [AllowAnonymous]
        public async Task<ActionResult> ResultProblem(string link)
        {
            var list= await GetInfoAboutResultAsync(link);
            return View(list);
        }
        public async Task<List<Mark>> GetMarkListAsync()
        {
            var u = await _userManager.GetUserAsync(HttpContext.User);
            var list = _dbContext.Marks.Where(x=>x.Id_Person==u.Id).ToList();
            return list;
        }
        public async Task<ActionResult> ToDo() {
            var list = new List<Exercise>();
            list =  _dbContext.Exercises.Where(x=>x.DateToDo>=DateTime.Now&&x.DateWhenShowForUsers<=DateTime.Now).ToList();
            var u = await _userManager.GetUserAsync(HttpContext.User);
            foreach (var exercise in list)
            {
                if (_dbContext.Marks.FirstOrDefault(x => x.Id_Person == u.Id && x.Id_Exercise == exercise.Id)!=null)
                {
                    exercise.isScored= true;
                }   
            }
            return View(list);
        } 

        public async Task<List<MarkWithExercise>> GetMarkWithExercisesListAsync()
        {
        
            var _Marklist = await GetMarkListAsync();
            var list=new List<MarkWithExercise>();
            foreach (var mark in _Marklist)
            {
                var item = new MarkWithExercise();
                item.Mark = mark;
                item.Exercise = _dbContext.Exercises.FirstOrDefault(x => x.Id == mark.Id_Exercise);
                list.Add(item);
            }
            return list;
        }

        public async Task<ActionResult> GetViewMarkWithExercise()
        {
            var list = await GetMarkWithExercisesListAsync();
            return  View(list);
        }
        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

     
        [HttpPost]
        public async Task<ActionResult> Evaluate(Guid ExerciseId)
        {
            var User = await _userManager.GetUserAsync(HttpContext.User);
            if (User != null && ExerciseId != null)
            {
                var exercise = await _dbContext.Exercises.FirstAsync(x => x.Id == ExerciseId);

                if (exercise != null && !exercise.isScored && exercise.DateToDo >= DateTime.Now)
                {
                    var mark = await _dbContext.Marks.FirstOrDefaultAsync(x => x.Id_Exercise == ExerciseId && x.Id_Person == User.Id);
                    if (mark == null)
                    {
                        if (await CheckIfUserResolveThisProblemAsync(User.Nick, exercise.Name))
                        {
                            var NewMark = new Mark();
                            NewMark.Id_Exercise = exercise.Id;
                            NewMark.Id_Person = User.Id;
                            NewMark.Score = exercise.Score;
                            await _dbContext.AddAsync(NewMark);
                            await _dbContext.SaveChangesAsync();

                            if (exercise.Score >= exercise.MaxScore * 0.5)
                            {
                                var marks = _dbContext.Marks.Where(x => x.Id_Exercise == exercise.Id);
                                var managmentPoint = await _dbContext.ManagmentPoints.OrderByDescending(x => x.DateWhenSet<exercise.DateWhenShowForUsers).FirstOrDefaultAsync();
                                if (managmentPoint!=null)
                                {
                                    if (marks.Count()==1)
                                    {
     
                                        exercise.Score = exercise.MaxScore * (managmentPoint.PointsForSecondPerson) / 100;
                                    }
                                    else if(marks.Count() == 2)
                                    {
                                        exercise.Score = exercise.MaxScore * (managmentPoint.PointsForThirdPerson) / 100;
                                    }
                                    else if(marks.Count() == 3)
                                    {
                                        exercise.Score = exercise.MaxScore * (managmentPoint.PointsForOtherPerson) / 100;
                                    }

                                }
                                 _dbContext.Exercises.Update(exercise);
                            }
                            await _dbContext.SaveChangesAsync();
                        }
                        return RedirectToAction("ToDo");
                    }
                }
                else
                {
                    return RedirectToAction("ToDo");
                }
            }
            return RedirectToAction("ToDo");

        }
        public async Task<List<string>> CheckEnglishSPOJ(string username)
        {
            var url = "http://www.spoj.com/users/" + username + "/";
            HttpClient hc = new HttpClient();
            HttpResponseMessage result = await hc.GetAsync(url);
            Stream stream = await result.Content.ReadAsStreamAsync();
            HtmlDocument doc = new HtmlDocument();
            doc.Load(stream);
            var list = new List<string>();
            var nodes=doc.DocumentNode.SelectNodes("//table").First();
                HtmlNodeCollection cells = nodes.SelectNodes("tr");
                if (cells == null) { }
                else
                {   
                foreach (HtmlNode cell in cells)
                {
                    HtmlNodeCollection kom = cell.SelectNodes("td");
                    if (kom == null)
                        continue;
                    foreach (HtmlNode item in kom)
                    {
                        list.Add(item.InnerText);
                    }
                }
            }
            return list;
        }
        public async Task<List<string>> CheckPolishSPOJ(string username)
        {
            var url = "https://pl.spoj.com/users/" + username+"/";
            HttpClient hc = new HttpClient();
            HttpResponseMessage result = await hc.GetAsync(url);
            Stream stream = await result.Content.ReadAsStreamAsync();
            HtmlDocument doc = new HtmlDocument();
            doc.Load(stream);


            var list = new List<string>();
            var nodes = doc.DocumentNode.SelectNodes(@"//*[@id=""content""]/div[1]/div[2]/table[1]").First();
            HtmlNodeCollection cells = nodes.SelectNodes("tr");
            if (cells == null) { }
            else
            {
                foreach (HtmlNode cell in cells)
                {
                    HtmlNodeCollection kom = cell.SelectNodes("td");
                    if (kom == null)
                        continue;
                    foreach (HtmlNode item in kom)
                    {
                        list.Add(item.InnerText);
                    }
                }
            }

            return list;
        }
        private async Task<bool> CheckIfUserResolveThisProblemAsync(string nick,string exerciseName)
        {
            var list = await CheckEnglishSPOJ(nick);
            if (list.Contains(exerciseName))
            {
                return true;
            }
            else
            {
                 list = await CheckPolishSPOJ(nick);
                if (list.Contains(exerciseName))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
