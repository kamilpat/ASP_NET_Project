﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LigaAlgorytmicznaSPOJNetCore.Database.Models;
using LigaAlgorytmicznaSPOJNetCore.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace LigaAlgorytmicznaSPOJNetCore.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser,ApplicationRole,string>
    {
        public ApplicationDbContext()
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("DbCoreConnectionString");
                //var connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=\"C:\\Users\\Kamil\\Desktop\\Nowy folder\\LigaAlgorytmicznaSPOJNetCore\\LigaAlgorytmicznaSPOJNetCore\\Data\\SpojDatabase.mdf\";Integrated Security=True;Connect Timeout=30";
                //var connectionString = "Data Source=DESKTOP-Q207H9E;Initial Catalog=SpojNetCoreDataBase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";



                optionsBuilder.UseSqlServer(connectionString);
            }
        }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }     
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Exercise> Exercises { get; internal set; }  
        public DbSet<ManagmentPoint> ManagmentPoints { get; set; }
    }
}
