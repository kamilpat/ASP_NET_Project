﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LigaAlgorytmicznaSPOJNetCore.Data.Migrations
{
    public partial class AddMaxScoreColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxScore",
                table: "Exercises",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxScore",
                table: "Exercises");
        }
    }
}
