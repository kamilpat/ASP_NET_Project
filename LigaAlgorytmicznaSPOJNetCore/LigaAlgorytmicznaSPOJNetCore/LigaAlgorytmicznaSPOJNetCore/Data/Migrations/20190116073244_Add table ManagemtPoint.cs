﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LigaAlgorytmicznaSPOJNetCore.Data.Migrations
{
    public partial class AddtableManagemtPoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ManagmentPoints",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateWhenSet = table.Column<DateTime>(nullable: false),
                    PointsForFirstPerson = table.Column<int>(nullable: false),
                    PointsForSecondPerson = table.Column<int>(nullable: false),
                    PointsForOtherPerson = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManagmentPoints", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ManagmentPoints");
        }
    }
}
