﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LigaAlgorytmicznaSPOJNetCore.Data.Migrations
{
    public partial class AddColumnDataWhenShowForStudents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Id_Person",
                table: "Marks",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Exercises",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateWhenShowForUsers",
                table: "Exercises",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateWhenShowForUsers",
                table: "Exercises");

            migrationBuilder.AlterColumn<string>(
                name: "Id_Person",
                table: "Marks",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Exercises",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
