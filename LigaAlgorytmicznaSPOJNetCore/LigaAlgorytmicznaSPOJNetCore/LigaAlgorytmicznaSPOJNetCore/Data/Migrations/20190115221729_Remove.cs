﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LigaAlgorytmicznaSPOJNetCore.Data.Migrations
{
    public partial class Remove : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropColumn(
                name: "Id_Address",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Id_Album",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "Pesel",
                table: "AspNetUsers",
                newName: "AlbumNumber");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date_Create",
                table: "Marks",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date_Create",
                table: "Marks");

            migrationBuilder.RenameColumn(
                name: "AlbumNumber",
                table: "AspNetUsers",
                newName: "Pesel");

            migrationBuilder.AddColumn<Guid>(
                name: "Id_Address",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Id_Album",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });
        }
    }
}
