#pragma checksum "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Areas\Identity\Pages\Account\LoginWithRecoveryCode.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "722cdca499d0b8833a7cb39de9b8e64b6c6185e6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Identity_Pages_Account_LoginWithRecoveryCode), @"mvc.1.0.razor-page", @"/Areas/Identity/Pages/Account/LoginWithRecoveryCode.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Areas/Identity/Pages/Account/LoginWithRecoveryCode.cshtml", typeof(AspNetCore.Areas_Identity_Pages_Account_LoginWithRecoveryCode), null)]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Areas\Identity\Pages\Account\_ViewImports.cshtml"
using LigaAlgorytmicznaSPOJNetCore.Areas.Identity.Pages.Account;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"722cdca499d0b8833a7cb39de9b8e64b6c6185e6", @"/Areas/Identity/Pages/Account/LoginWithRecoveryCode.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6bf587992bd77a75991de403d21cef7cb965a620", @"/Areas/Identity/Pages/Account/_ViewImports.cshtml")]
    public class Areas_Identity_Pages_Account_LoginWithRecoveryCode : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Areas\Identity\Pages\Account\LoginWithRecoveryCode.cshtml"
  
    ViewData["Title"] = "Recovery code verification";

#line default
#line hidden
            BeginContext(104, 6, true);
            WriteLiteral("\r\n<h2>");
            EndContext();
            BeginContext(111, 17, false);
#line 7 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Areas\Identity\Pages\Account\LoginWithRecoveryCode.cshtml"
Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(128, 781, true);
            WriteLiteral(@"</h2>
<hr />
<p>
    You have requested to log in with a recovery code. This login will not be remembered until you provide
    an authenticator app code at log in or disable 2FA and log in again.
</p>
<div class=""row"">
    <div class=""col-md-4"">
        <form method=""post"">
            <div asp-validation-summary=""All"" class=""text-danger""></div>
            <div class=""form-group"">
                <label asp-for=""Input.RecoveryCode""></label>
                <input asp-for=""Input.RecoveryCode"" class=""form-control"" autocomplete=""off"" />
                <span asp-validation-for=""Input.RecoveryCode"" class=""text-danger""></span>
            </div>
            <button type=""submit"" class=""btn btn-default"">Log in</button>
        </form>
    </div>
</div>
 
");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(928, 52, true);
                WriteLiteral("\r\n    <partial name=\"_ValidationScriptsPartial\" />\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<LoginWithRecoveryCodeModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<LoginWithRecoveryCodeModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<LoginWithRecoveryCodeModel>)PageContext?.ViewData;
        public LoginWithRecoveryCodeModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
