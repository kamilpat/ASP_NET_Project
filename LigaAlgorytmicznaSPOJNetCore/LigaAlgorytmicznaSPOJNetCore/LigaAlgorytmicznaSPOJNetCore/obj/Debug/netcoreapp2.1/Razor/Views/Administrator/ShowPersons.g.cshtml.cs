#pragma checksum "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1ec9b383e10d44772d9875356e80d500060ea7e9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Administrator_ShowPersons), @"mvc.1.0.view", @"/Views/Administrator/ShowPersons.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Administrator/ShowPersons.cshtml", typeof(AspNetCore.Views_Administrator_ShowPersons))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\_ViewImports.cshtml"
using LigaAlgorytmicznaSPOJNetCore;

#line default
#line hidden
#line 2 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\_ViewImports.cshtml"
using LigaAlgorytmicznaSPOJNetCore.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1ec9b383e10d44772d9875356e80d500060ea7e9", @"/Views/Administrator/ShowPersons.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e63d912705cb29981380f85e09a4b82aca604432", @"/Views/_ViewImports.cshtml")]
    public class Views_Administrator_ShowPersons : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<System.Collections.Generic.List<LigaAlgorytmicznaSPOJNetCore.Models.ApplicationUser>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
  
    ViewBag.Title = "Program";
    Layout = "~/Views/Shared/MenuLayout.cshtml";

#line default
#line hidden
            BeginContext(182, 485, true);
            WriteLiteral(@"<div class=""col-md-12 col-sm-12 col-xs-12"">

    <h2>Lista Członków</h2>

    <div class=""container"">
        <table class=""table table-striped"">
            <thead>
                <tr>
                    <th>Nazwisko</th>
                    <th>Imie</th>
                    <th>Nick</th>
                    <th>Płeć</th>
                    <th>Akceptuj</th>
                    <th>Odrzuc</th>


                </tr>
            </thead>
            <tbody>
");
            EndContext();
#line 25 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                 foreach (var person in Model.OrderBy(x => x.isConfirmed))
                {

#line default
#line hidden
            BeginContext(762, 54, true);
            WriteLiteral("                    <tr>\r\n                        <td>");
            EndContext();
            BeginContext(817, 15, false);
#line 28 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                       Write(person.LastName);

#line default
#line hidden
            EndContext();
            BeginContext(832, 35, true);
            WriteLiteral("</td>\r\n                        <td>");
            EndContext();
            BeginContext(868, 16, false);
#line 29 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                       Write(person.FirstName);

#line default
#line hidden
            EndContext();
            BeginContext(884, 65, true);
            WriteLiteral("</td>\r\n                        <td>\r\n                            ");
            EndContext();
            BeginContext(950, 121, false);
#line 31 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                       Write(Html.ActionLink(person.Nick, "UserInformationAndProblemResolvedAsync", "Administrator", new { nick = person.Nick }, null));

#line default
#line hidden
            EndContext();
            BeginContext(1071, 35, true);
            WriteLiteral("\r\n                        </td>\r\n\r\n");
            EndContext();
#line 34 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                         if (@person.Gender == "Man")
                        {

#line default
#line hidden
            BeginContext(1188, 48, true);
            WriteLiteral("                            <td>Mężczyzna</td>\r\n");
            EndContext();
#line 37 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                        }
                        else
                        {

#line default
#line hidden
            BeginContext(1320, 46, true);
            WriteLiteral("                            <td>Kobieta</td>\r\n");
            EndContext();
#line 41 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"

                        }

#line default
#line hidden
            BeginContext(1395, 30, true);
            WriteLiteral("                        <td>\r\n");
            EndContext();
#line 44 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                             if (!person.isConfirmed)
                            {


                                using (Html.BeginForm("AddToGroup", "Administrator", new { idToAdd = person.Id }, FormMethod.Post))
                                {

#line default
#line hidden
            BeginContext(1683, 205, true);
            WriteLiteral("                                    <div class=\"form-group-xs\">\r\n                                        <input type=\"submit\" value=\"+\" class=\"xsbtn-accept\" />\r\n                                    </div>\r\n");
            EndContext();
#line 53 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                                }
                            }

#line default
#line hidden
            BeginContext(1954, 63, true);
            WriteLiteral("\r\n                        </td>\r\n                        <td>\r\n");
            EndContext();
#line 58 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                             if (!person.isConfirmed)
                            {


                                using (Html.BeginForm("DeleteUserFromGroup", "Administrator", new { idToDelete = person.Id }, FormMethod.Post))
                                {

#line default
#line hidden
            BeginContext(2287, 161, true);
            WriteLiteral("                                    <div class=\"form-group-xs\">\r\n                                        <input type=\"submit\" value=\"-\" class=\"xsbtn-reject\" />\r\n");
            EndContext();
            BeginContext(2541, 44, true);
            WriteLiteral("                                    </div>\r\n");
            EndContext();
#line 68 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"

                                }


                            }
                            else
                            {
                                using (Html.BeginForm("DeleteUserFromGroup", "Administrator", new { idToDelete = person.Id }, FormMethod.Post))
                                {

#line default
#line hidden
            BeginContext(2902, 164, true);
            WriteLiteral("                                    <div class=\"form-group-xs\">\r\n                                        <input type=\"submit\" value=\"Usuń\" class=\"xsbtn-reject\" />\r\n");
            EndContext();
            BeginContext(3159, 44, true);
            WriteLiteral("                                    </div>\r\n");
            EndContext();
#line 81 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                                }
                            }

#line default
#line hidden
            BeginContext(3269, 60, true);
            WriteLiteral("\r\n                        </td>\r\n                    </tr>\r\n");
            EndContext();
#line 86 "C:\Users\Kamil\Desktop\SPOJNAJNAJ\Nowy folder\LigaAlgorytmicznaSPOJNetCore\LigaAlgorytmicznaSPOJNetCore\Views\Administrator\ShowPersons.cshtml"
                }

#line default
#line hidden
            BeginContext(3348, 70, true);
            WriteLiteral("\r\n\r\n            </tbody>\r\n        </table>\r\n\r\n\r\n\r\n</div>\r\n    </div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<System.Collections.Generic.List<LigaAlgorytmicznaSPOJNetCore.Models.ApplicationUser>> Html { get; private set; }
    }
}
#pragma warning restore 1591
