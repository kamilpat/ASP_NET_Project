﻿using System;
using LigaAlgorytmicznaSPOJNetCore.Database.Models;
using LigaAlgorytmicznaSPOJNetCore.Models;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public class StudentAndMark
    {
        public ApplicationUser User { get; set; }
        public Mark Mark { get; set; }
        public string NameExcercise { get; set; }

        public Guid IdExercise { get; set; }
}
}