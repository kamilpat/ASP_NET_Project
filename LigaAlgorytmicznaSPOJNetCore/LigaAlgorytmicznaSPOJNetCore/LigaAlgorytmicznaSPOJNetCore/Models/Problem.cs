﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{

    public class Problem
    {

            public string Id { get; set; }
            public string Name { get; set; }
            public string Quality { get; set; }
            public string Users { get; set; }
            public string Acc { get; set; }
            public string Difficulty { get; set; }
            public string Link { get; set; }


        
    }
}