﻿using System.ComponentModel.DataAnnotations;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public enum Gender
    {
       
        [Display(Name = "Kobieta")]
        Woman,
        [Display(Name = "Mężczyzna")]
        Man
    }
    
}