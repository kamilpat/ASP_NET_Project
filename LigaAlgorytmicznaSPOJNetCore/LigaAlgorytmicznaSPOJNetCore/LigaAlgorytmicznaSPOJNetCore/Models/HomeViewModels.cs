﻿using System.ComponentModel.DataAnnotations;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public class IndexViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj moje dane logowania")]
        public bool RememberMe { get; set; }
    }
    public class ResetPasswordViewModel
{
    [Required]
    public string UserName { get; set; }
    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; }
    [Required]
    [DataType(DataType.Password)]
    public string ConfirmPassword { get; set; }
    [Required]
    public string Token { get; set; }
}
    }
