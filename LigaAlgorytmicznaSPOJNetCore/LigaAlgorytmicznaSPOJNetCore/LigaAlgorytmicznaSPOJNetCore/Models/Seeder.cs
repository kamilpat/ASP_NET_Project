﻿using LigaAlgorytmicznaSPOJNetCore.Data;
using LigaAlgorytmicznaSPOJNetCore.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

public  class SeedData
{
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly UserManager<ApplicationUser> _userManager;
    public SeedData(
        UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager
    )
    {
        _userManager = userManager;
        _signInManager = signInManager;
    }

    public  async System.Threading.Tasks.Task InitializeDataBaseRoleAndAdministrator(IServiceProvider serviceProvider)
    {
       

    
        var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
     //   context.Database.EnsureCreated();
        if (!context.Users.Any())
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

            var user = new ApplicationUser
            {
                UserName = "admin@gmail.com",
                FirstName = "Administrator",
                LastName = "Administrator",
                Gender = "Man",
                BirthDate = DateTime.Today,
                PhoneNumber = "999999999",
                Email = "admin@gmail.com",
                AlbumNumber = "000123",
                Nick = "",
            };
           var roleResult = await RoleManager.CreateAsync(new ApplicationRole("Administrator"));
             roleResult = await RoleManager.CreateAsync(new ApplicationRole("Student"));

            var result = await  _userManager.CreateAsync(user, "Admin12+");
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Administrator");
                context.SaveChanges();
                await _signInManager.SignInAsync(user, false);
            }
            context.SaveChanges();
        }
    }
}