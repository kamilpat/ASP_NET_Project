﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public class ApplicationUser:IdentityUser
    {
        public ApplicationUser() : base (){}
        public string Nick { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AlbumNumber { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? LastTimeLogin { get; set; }
        public bool isConfirmed { get; set; }


    }
}
