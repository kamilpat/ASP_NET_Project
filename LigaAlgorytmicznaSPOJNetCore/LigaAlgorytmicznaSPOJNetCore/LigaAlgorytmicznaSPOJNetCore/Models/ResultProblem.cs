﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public class ResultProblem
    {
     
            public string Id { get; set; }
            public string Date { get; set; }
            public string Problem { get; set; }
            public string Result { get; set; }
            public string Time { get; set; }
            public string Memory { get; set; }
            public string Lang { get; set; }
        
    }
}