﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public class ProblemWithLink
    {
       
            public string Text { get; set; }
            public string Link { get; set; }

            public ProblemWithLink()
            {
            }
            public ProblemWithLink(string text, string link)
            {
                Text = text;
                Link = link;
            }
        
    }
}