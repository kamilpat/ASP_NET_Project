﻿
using LigaAlgorytmicznaSPOJNetCore.Database.Models;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public class MarkWithExercise
    {
        public Mark Mark      { get; set; }
        public Exercise Exercise { get; set; }  
    }
}