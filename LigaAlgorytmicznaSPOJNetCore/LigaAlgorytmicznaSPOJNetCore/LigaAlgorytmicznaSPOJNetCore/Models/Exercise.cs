﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    public class Exercise
    {
        public Guid Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateToDo { get; set; }       
        [DataType(DataType.DateTime)]
        public DateTime DateWhenShowForUsers { get; set; }


        [Required]
        public string Name { get; set; }
        public bool isComplete { get; set; }
        public bool isScored { get; set; }

        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Maksymalna ilość punktów jaką można dostać musi być liczbą całkowitą ")]
        public int MaxScore { get; set; }
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Ilość punktów jaką można dostać musi być liczbą całkowitą ")]

        public int Score { get; set; }

        public Exercise()
        {
            Id = Guid.NewGuid();

        }
    }
}