﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigaAlgorytmicznaSPOJNetCore.Data
{
    public class ManagmentPoint
    {
        public Guid Id { get; set; }
        public DateTime DateWhenSet { get; set; }    
        public int PointsForFirstPerson { get; set; }
        public int PointsForSecondPerson { get; set; }
        public int PointsForThirdPerson { get; set; }
        public int PointsForOtherPerson { get; set; }
    }
}
