﻿using LigaAlgorytmicznaSPOJNetCore.Data;
using LigaAlgorytmicznaSPOJNetCore.Database.Models;
using LigaAlgorytmicznaSPOJNetCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{

    public static class IdentityExtensions
    {
       static ApplicationDbContext _dbcontext =new ApplicationDbContext();
        public static string GetUserFirstname(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("FirstName");
            return (claim != null) ? claim.Value : string.Empty;
        }
        public static string GetUserNick(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Nick");
            return (claim != null) ? claim.Value : string.Empty;
        }
        public static bool GetUserisConfirmed(this IIdentity identity)
        {
           var claim= _dbcontext.Users.FirstOrDefault(x=>x.Email==identity.Name);
            if (claim==null)
            {
                return false;
            }
            else
            {
                return claim.isConfirmed;
            }
        }
    }

}