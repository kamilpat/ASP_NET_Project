﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using LigaAlgorytmicznaSPOJNetCore.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace LigaAlgorytmicznaSPOJNetCore.Models
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }
        

        public class InputModel
        {
            public bool isTrue
            { get { return true; } }
            [Display(Name = "Akceptuję regulamin i zezwalam na przetwarzanie i wyświetlanie moich danych osobowych potrzeby Systemu Ligi Algorytmicznej.")]
            //      [Range(typeof(bool), "false", "true", ErrorMessage = "Musisz wydać zgodę na przetwarzanie danych osobowych")]
            [Compare("isTrue", ErrorMessage = "Musisz wydać zgodę na przetwarzanie danych osobowych")]

            public bool TermsAndConditions { get; set; }
            [Display(Name = "Zapoznałem się akceptuję regulamin Ligi Algorytmicznej.")]
            [Compare("isTrue", ErrorMessage = "Musisz zaakceptować regulamin")]
            // [Range(typeof(bool), "true", "true", ErrorMessage = "Musisz zaakceptować regulamin")]
            public bool Rules { get; set; }
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Hasło")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Potwierdź Hasło")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }





            [Required]
            [Display(Name = "Nick z SPOJ")]
            
            public string Nick { get; set; }
            [Display(Name = "Imie")]
            [Required]

            public string FirstName { get; set; }
            [Display(Name = "Nazwisko")]
            [Required]

            public string LastName { get; set; }
            [Display(Name = "Numer albumu")]
            [Required]
            [RegularExpression("([0-9]+)",ErrorMessage ="Numer albumu musi składać się z cyfr")]
            [StringLength(100, ErrorMessage = "Numer albumu musi mieć maksymalnie {0} cyfr i minimalnie {1}", MinimumLength = 2)]

            public string Id_Album { get; set; }
            [Display(Name = "Płeć")]
          
            [Required]

            public Gender Gender { get; set; }
            [Display(Name = "Data urodzenia")]
            [Required]
            [DataType(DataType.Date)]

            public DateTime? BirthDate { get; set; }
         
            [Display(Name = "Numer telefonu")]

            public string PhoneNumber { get;  set; }
          
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");





























            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = Input.Email, Email = Input.Email };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return LocalRedirect(returnUrl);
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
