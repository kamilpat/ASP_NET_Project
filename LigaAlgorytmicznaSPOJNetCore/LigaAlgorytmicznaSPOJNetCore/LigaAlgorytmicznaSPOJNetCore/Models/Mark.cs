﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LigaAlgorytmicznaSPOJNetCore.Database.Models
{
    public class Mark
    {
        public Guid Id { get; set; }
        [Required]
        public Guid Id_Exercise { get; set; }
        [Required]
        public string Id_Person { get; set; }
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Wynik musi składać się tylko z cyfr")]

        public int Score { get; set; }

        public DateTime? Date_Create { get; set; }

        public Mark()
        {
            Id=Guid.NewGuid();
            Date_Create = DateTime.Now;
        }
        

    }
}